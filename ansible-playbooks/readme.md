# Create a host

To release the track hub facade software, you need a host, such as an 
Amazon Web Services EC2 virtual machine instance. 

To create a new host in your AWS account, run playbook `aws.yml`.

To run the `aws.yml` playbook:

1. Create file aws_vars.yml. See `example_aws_vars.yml`.
2. Run playbook `aws.yml` using the command: `ansible-playbook aws.yml`.

The first time you run this, ansible will create a new private key for the
host and save it in your local computer's `.ssh` directory. Also, ansible
will add public keys in the `pubkey` role's `files` folder to the host's
`authorized_hosts` file. Delete those keys if you don't want them to be added.
To remove a key once it's been added, copy the key into the `Delete` folders.

# Install / update software on the host.

To install / update software on a host, use playbook `setup.yml`.

To run the `setup.yml` playbook:

1. Create file `setup_vars.yml`. See `example_setup_vars.yml`.
2. Create an inventory file listing host (or hosts). See `example_inventory.ini`.
3. Run playbook `setup.yml` with `ansible-playbook -i [inventory file name] setup.yml`.

After running playbook `setup.yml`, enter the host URL in your Web browser to see
a user interface for the track hub. 

The inventory file lists hosts that need to be provisioned / updated. 



Example:

```
[hub_facade_hosts]
hub1 ansible_host=123.456.789 domain=www.example1.com secret_key=djkdfjdfjjd
hub2 ansible_host=987.654.321 domain=www.example2.com secret_key=ncncnxxmzsd

[hub_facade_hosts:vars]
ansible_ssh_common_args="-o StrictHostKeyChecking=no"
ansible_python_interpreter=/usr/bin/python3
```

where `secret_key` is a random string of ASCII characters you make up, used
by hub facade's Django framework.