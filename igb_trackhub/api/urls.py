from django.urls import path, re_path
from django.views.decorators.csrf import csrf_exempt
from . import views

urlpatterns = [
    path(r'', views.get_resources, name='get-resources'),
    path(r'igbGenomeVersions', csrf_exempt(views.igb_genome_versions), name='igb-genome-versions')
]
