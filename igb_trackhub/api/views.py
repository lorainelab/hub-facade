from django.http import HttpResponse
from .create_resources import CreateResources
import json


# Create your views here.
cr = CreateResources()


def get_resources(request):
    if request.method == 'GET':
        hub_url = request.GET.get('hubUrl', '')
        file_path = str(request.GET.get('filePath'))
        file_path_components = file_path.split("/")
        if len(file_path_components) > 2:
            genome = file_path_components[-2]
        else:
            genome = ""
        if "/contents.txt" in file_path:
            res = cr.create_contents_txt(hub_url)
        elif "genome.txt" in file_path:
            if genome == "":
                res = ""
            else:
                res = cr.create_genome_txt(hub_url, genome)
        elif "annots.xml" in file_path:
            if genome == "":
                res = ""
            else:
                res = cr.create_annots_xml(hub_url, genome)
            return HttpResponse(res, content_type="text/xml")
        elif "species.txt" in file_path:
            res = cr.create_species_txt(hub_url)
        elif "data_extensions.log" in file_path:
            res = str(cr.unique_extensions)
        elif len(hub_url) > 0 and len(file_path.strip("/")) == 0:
            res = f"This URL is parsed by IGB in order to utilize UCSC trackhub data for the hub linked to <a href='{hub_url}'>here</a>.<br>To use it, add it as a quickload data source in IGB. Instructions for how to do this can be found <a href='https://wiki.bioviz.org/confluence/display/igbman/Adding+and+Managing+Data+Source+Servers'>here</a>."
        else:
            res = "ERROR: No such endpoint exists.\n"

        return HttpResponse(res, content_type="text/html")

def igb_genome_versions(request):
    error = {}
    if request.method == 'POST':
        try:
            ucsc_genomes = json.loads(request.body.decode('utf-8')).get('ucscGenomes')
            if ucsc_genomes:
                return HttpResponse(cr.get_igb_genome_versions(ucsc_genomes), content_type="application/json")
            else:
                error['Error'] = 'The ucscGenomes parameter required and missing.'
        except:
            error['Error'] = 'The request body is malformed. It should be a JSON object with a ucscGenomes property.'
    else:
        error['Error'] = 'This endpoint only supports the POST method.'
    return HttpResponse(json.dumps(error), content_type="application/json")
