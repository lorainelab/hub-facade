# Track Hub Facade API

---

## Running

### Create and enter a virtual environment for python3

`virtualenv -p python3 venv`

`. venv/bin/activate`

### Install dependencies

`pip install -r requirements.txt`

### Start the server

`cd igb_trackhub/`

`python manage.py runserver`

---

### Configuration

To run locally, you must configure Django to allow connections from localhost. Modify `igb_trackhub/igb_trackhub/settings.py` to include the change below:

`ALLOWED_HOSTS = ['127.0.0.1', 'localhost']`

During local development API calls are made to:

`http://127.0.0.1:8000/api`

### About

IGB's quickload system loads data sources under the assumption that their associated metadata are findable 
under a Quickload-specific URL and directory structure. Track hub data distribution sites are structured
similarly, but using different syntax and resource (URL) names. The translate.bioviz.org app converts between
the two languages. It acts like a facade, providing services and metadata IGB requires so that IGB users can
visualize and interact with data from Track Hub data sharing sites.

We hope this application will make it easier for researchers and the public to visualize, understand, and 
investigate genomic data sets. 

For ease-of-use for programmers, we implemented a simple endpoint syntax, described below.

### Endpoints

Syntax:

* $(HUB_URL) - the URL of the Track Hub's "hub.txt" metadata file.
* $(DOMAIN) - root address of the Web site where hub facade software is deployed

#### contents.txt:
##### Type: GET

- `https://$(DOMAIN)/api/?hubUrl=$(HUB_URL)&filePath=/contents.txt`
- Returns a list of available genome versions for a given track hub
- Example: https://translate.bioviz.org/api/?hubUrl=https://cgl.gi.ucsc.edu/data/cactus/363-avian-2020-hub/hub.txt&filePath=/contents.txt
- Details: Obtain a contents.txt string to allow IGB to determine the genomes available in the data source. Genome names are UCSC-derived. Column one holds genome names and column two holds genome descriptions. In cases where a UCSC genome has a matching IGB synonym, the default IGB genome description is used. Otherwise, if the hub provides a genome description, that is used; if it isn't, an external API is used to obtain it.

#### species.txt:
##### Type: GET

- `https://$(DOMAIN)/api/?hubUrl=$(HUB_URL)&filePath=/species.txt`
- Returns a list of available species versions for a given track hub
- Example: https://translate.bioviz.org/api/?hubUrl=https://cgl.gi.ucsc.edu/data/cactus/363-avian-2020-hub/hub.txt&filePath=/species.txt
- Details: Obtain a species.txt string to allow IGB to associate particular genomes with their respective species. Genomes for which this information cannot be found are loaded in IGB under the species 'Trackhub misc'.

#### genome.txt:
##### Type: GET

- `https://$(DOMAIN)/api/?hubUrl=$(HUB_URL)&filePath=/$(GENOME_VERSION)/genome.txt`
- Returns a list of chromosome names and their sequence locations for a given genome version and track hub
- Example: https://translate.bioviz.org/api/?hubUrl=https://cgl.gi.ucsc.edu/data/cactus/363-avian-2020-hub/hub.txt&filePath=/Aegithalos_caudatus/genome.txt
- Details: Obtain a genomes.txt string to allow loading chromosome information for a particular genome in IGB. If a genome.txt is requested for a genome already in the default IGB synonyms.txt - and therefore one loaded by a default IGB data source, which contains its own genome.txt - an empty response is returned to prevent chromosome info duplication. This assumes that the default synonyms.txt is always loaded in IGB, regardless of whether or not these data sources are enabled. Build and return genomes.txt only if a custom reference genomic sequence (2bit URL) is provided. For cases in which it is not, it is assumed that the UCSC genome name matches an IGB synonym. If it doesn't, as there is no sense in loading a trackhub without a reference sequence, an empty response is returned.

#### annots.xml:
##### Type: GET

- `https://$(DOMAIN)/api/?hubUrl=$(HUB_URL)&filePath=/$(GENOME_VERSION)/annots.xml`
- Returns the data files available for loading in IGB for a given genome version and track hub
- Example: https://translate.bioviz.org/api/?hubUrl=https://cgl.gi.ucsc.edu/data/cactus/363-avian-2020-hub/hub.txt&filePath=/Aegithalos_caudatus/annots.xml
- Details: Obtain an annots.xml string to allow IGB to determine the data and its locations for a particular genome. The genome name IGB uses to detail requests for genomes.txt and annots.xml. The genome name used to detail requests for annots.xml is based on UCSC trackhub information, however, it may coincide with an official IGB name. IGB genome names are described in the first column of the default synonyms.txt.

#### igbGenomeVersion:
##### Type: POST

- `https://$(DOMAIN)/api/igbGenomeVersions`
  - Request body: { `ORGANISM_NAME`: [ `GENOME1`, `GENOME2`, `...` ] }
- Returns the IGB equivalent for the UCSC genome names, if found.
- Example: `https://translate.bioviz.org/api/igbGenomeVersions`
	- Example request body: 
		```{
			"ucscGenomes": [
			  {
			  "Dromaius novaehollandiae": [
				  "GCF_003342905.1",
				  "GCA_016128335.1"
			  ]
			  }
		  ]
	  }```
- Details: A method for determining whether or not a UCSC genome name can be mapped to an IGB genome version in order to decide which genome versions in the trackhub table UI can be made to have the option of utilizing IGB's 'open genome' endpoint located at http://localhost:7085/IGBControl?version=GENOME.
---

## Testing

### Watch requests made by IGB

Monitor all outgoing HTTP traffic on a machine (assumes your active outbound interface is en0):

`tcpdump -A -i en0 'tcp[((tcp[12:1] & 0xf0) >> 2):4] = 0x47455420'`

### List unique data file extensions

- `https://$(DOMAIN)/api/?filePath=/data_extensions.log`
  - The returned set is updated every time the annots.txt endpoint is hit, from when the server was started.

---

## Deploying

See the `ansible-playbooks` folder for the configurations describing deployment.
